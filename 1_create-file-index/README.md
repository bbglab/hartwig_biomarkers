# 1_create-file-index

#### Purpose 
* Create organized storage for file path names in Hartwig.
* File paths are fed as inputs to subsequent pipelines. 

#### Input
* Clinical sampleIds for the clinical data set. 

#### Output 
* Separate text files containing lists of file paths across sampleIds; for somatic, cnv, cnv_gene, purity, neoepitope, driver, linx, sv, and isofox data. 
