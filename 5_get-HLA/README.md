# 5_get-HLA

## get_hla-features.ipynb
* purpose: create dataframe of hla features hla dictionary
* input: processed input file from Claudia, map of HLA supertypes, hla helper file. 
* output: dataframe with each row corresponding to a sampleId and columns being hla features. 

## get_lilac-features.ipynb
* purpose: create formatted dataframe of hla features (lilac) shared by Fran
* input: lilac file shared by Fran
* output: dataframe with each row corresponding to a sampleId and columns being lilac features.
