# 6_get-svs

## get_SV-features
* purpose: compute structural variant features extract from linx directories.
* input: linx directory locations 
* output: data frame where each rown corresponds to a sampleId and structural variant features on the columns.
