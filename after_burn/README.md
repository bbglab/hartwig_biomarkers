# afterburn

* folder is for joining and cleaning data output by pipeline.

## combine
* purpose: join all files based on sampleId
* input: all output features files from the pipeline
* output: large dataframe containing all biomarkers from the pipeline together
