wd <- dirname(getwd())
source(paste0(wd,"/mission_control/treasure_map.R"))
source(paste0(wd,"/mission_control/cibersort_help.R"))

library(e1071)
library(parallel)
library(dplyr)
args <- commandArgs(trailing = TRUE) ## "RawTPM" or "AdjTPM" #args = list("RawTPM")

mixture <- read.csv(paste0(TMP_DIR, "cibersort_prep_", args[1], ".csv"))
sig_ref <- read.csv(paste0(REF_DIR, "LM22.txt"), sep = "\t")

mixture <- mixture[which(mixture$GeneName %in% sig_ref$Gene.symbol),]
sig_ref <- sig_ref[which(sig_ref$Gene.symbol %in% mixture$GeneName),]
X <- data.matrix(sig_ref[,-1])
Y <- data.matrix(mixture[,-1])

out <- CIBERSORT_simple(X, Y, QN = FALSE, scale_X = TRUE, scale_Y = FALSE)

cibersort_features <- 
    (out$wts 
        %>% left_join(out$stats, by = "sampleId") 
        %>% rename_at(vars(-sampleId), ~ paste0("cibersort_", .x))
        %>% relocate(sampleId))
write.csv( cibersort_features, file = paste0(TMP_DIR, "cibersort_", args[1], "_ready.csv"), row.names=FALSE)
